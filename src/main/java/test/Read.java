package test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code39Reader;

public class Read {

    public static void main(String[] args) throws Exception {
        
        File file = new File("D:/Users/frank/TSMC_Work/barcode/doc/sample1.jpg");
        InputStream barCodeInputStream = new FileInputStream(file);
        BufferedImage barCodeBufferedImage = ImageIO.read(barCodeInputStream);

        LuminanceSource source = new BufferedImageLuminanceSource(barCodeBufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        bitmap = bitmap.crop(2300, 5, 1100, 295);
        
//        BitMatrix matrix = bitmap.getBlackMatrix();
//        int height = bitmap.getHeight();
//        for (int i = 0; i < bitmap.getWidth(); i++) {
//            boolean isBlack = matrix.get(i, height / 2);
//            if (isBlack) {
//                System.out.println(i + ": B");
//            } else {
//                System.out.println(i + ": W");
//            }
//        }
        
        
        Reader reader = new Code39Reader();
        Result result = reader.decode(bitmap);
        System.out.println("file: " + file.getName() + ", Barcode text is " + result.getText());
    
    }
    
}
