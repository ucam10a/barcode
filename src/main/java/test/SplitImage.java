package test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class SplitImage {

    public static BufferedImage getPage(String path, int num) throws IOException {
        ImageInputStream is = ImageIO.createImageInputStream(new File(path));
        if (is == null || is.length() == 0){
          // handle error
        }
        Iterator<ImageReader> iterator = ImageIO.getImageReaders(is);
        if (iterator == null || !iterator.hasNext()) {
          throw new IOException("Image file format not supported by ImageIO: " + path);
        }
        // We are just looking for the first reader compatible:
        ImageReader reader = (ImageReader) iterator.next();
        iterator = null;
        reader.setInput(is);
        
        int nbPages = reader.getNumImages(true);
        if (num > nbPages) {
            throw new RuntimeException("total page is " + nbPages + ", " + num + " is over!");
        }
        return reader.read(num);
    }
    
    public static void main(String[] args) throws Exception {
        
        String pathToImage = "";
        getPage(pathToImage, 1);
        
    }
    
}
