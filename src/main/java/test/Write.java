package test;

import java.io.File;
import java.io.FileOutputStream;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code39Writer;

public class Write {

    public static void main(String[] args) throws Exception {
        
        //String text = "0123456789"; // this is the text that we want to encode
        String text = "4500046299"; // this is the text that we want to encode

        int width = 200;
        int height = 56; // change the height and width as per your requirement

        // (ImageIO.getWriterFormatNames() returns a list of supported formats)
        String imageFormat = "png"; // could be "gif", "tiff", "jpeg" 

        BitMatrix bitMatrix = new Code39Writer().encode(text, BarcodeFormat.CODE_39, width, height);
        
        
//        for (int i = 0; i < width; i++) {
//            boolean isBlack = bitMatrix.get(i, 28);
//            if (isBlack) {
//                System.out.println(i + ": B");
//            } else {
//                System.out.println(i + ": W");
//            }
//        }
        
        MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, new FileOutputStream(new File("D:/Users/frank/TSMC_Work/barcode/doc/test.png")));
    
    }
    
}
